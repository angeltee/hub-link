/**
* Project: yui3-common-hub
 * Class HubDataToDtoUtils
 * Version 1.0
 * File Created at 2018年8月17日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.utils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import yui.comn.hub.model.HubJavaType;

/**
 * 把map对象解析成dto对象工具类
 * @author yuyi (1060771195@qq.com)
 */
public class HubDataToDtoUtils {
 
    public static Object toDistTypeVal(Class<?> type, Object val) {
        String clazzTypeName = type.getName();
        String valTypeName = val.getClass().getTypeName();

        if (StringUtils.equals(clazzTypeName, valTypeName)) {
            return val;
        }
        switch (HubJavaType.getJavaTypeByName(clazzTypeName)) {
        	case TYPE_STRING:
        		return toDefString(val);
            case TYPE_DATE:
                return toDateByString(val.toString());
            case TYPE_TIMESTAMP:
                return toTimestampByString(val.toString());
            case TYPE_SQL_DATE:
                return toSqlDateByString(val.toString());
            case TYPE_LONG:
            case TYPE_BASE_LONG:
                return Long.valueOf(val.toString());
            case TYPE_INT:
            case TYPE_BASE_INT:
                return Integer.valueOf(val.toString());
            case TYPE_DOUBLE:
            case TYPE_BASE_DOUBLE:
                return Double.valueOf(val.toString());
            case TYPE_FLOAT:
            case TYPE_BASE_FLOAT:
                return Float.valueOf(val.toString());
            case TYPE_BIGDECIMAL:
                return new BigDecimal(val.toString());
            default:
                return val;
            }
    }
    
    public static String toDefString(Object val) {
    	return val.toString();
    }
    
    //转化为timestamp
    public static Object toTimestampByString(String dateStr) {
        Timestamp result = DateUtils.formatTimestamp(dateStr, DateUtils.FULL_ST_FORMAT);
        if (null == result) {
            result = DateUtils.formatTimestamp(dateStr, DateUtils.ST_FORMAT);
        }
        if (null == result) {
            result = DateUtils.formatTimestamp(dateStr, DateUtils.CURRENCY_J_FORMAT);
        }
        return result;
    }
    
    //转化为sqlDate
    public static Object toSqlDateByString(String dateStr) {
        java.sql.Date result = DateUtils.formatSqlDate(dateStr, DateUtils.FULL_ST_FORMAT);
        if (null == result) {
            result = DateUtils.formatSqlDate(dateStr, DateUtils.ST_FORMAT);
        }
        if (null == result) {
            result = DateUtils.formatSqlDate(dateStr, DateUtils.DAY_FORMAT);
        }
        return result;
    }
    
    //转化为sqlDate
    public static Object toDateByString(String dateStr) {
        Date result = DateUtils.formatDate(dateStr, DateUtils.FULL_ST_FORMAT);
        if (null == result) {
        	result = DateUtils.formatDate(dateStr, DateUtils.DAY_FORMAT);
        }
        if (null == result) {
        	result = DateUtils.formatDate(dateStr, DateUtils.CURRENCY_J_DATA_FORMAT);
        }
        if (null == result) {
            result = DateUtils.formatDate(dateStr, DateUtils.ST_FORMAT);
        }
        if (null == result) {
        	result = DateUtils.formatDate(dateStr, DateUtils.FULL_ST_FORMAT);
        }
        return result;
    }
    
}
