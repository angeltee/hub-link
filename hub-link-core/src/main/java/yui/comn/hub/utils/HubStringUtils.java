/**
* Project: yui3-common-hub
 * Class HubXmlUtils
 * Version 1.0
 * File Created at 2018年8月12日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.utils;

import org.apache.commons.lang3.StringUtils;

import yui.comn.hub.model.Symbol;

/**
 * xml 数据解析 工具类
 * @author yuyi (1060771195@qq.com)
 */
public class HubStringUtils {
 
    /**
     * 得到get方法名 SysUser --> isSysUser
     */
    public static String getIsMethodName(String str) {
        if (StringUtils.startsWithIgnoreCase(str, "is")) {
            return StrUtils.lowerCaseFirstChar(str);
        }
        
        return StrUtils.upperCaseFirstCharAndAddPrefix(str, "is");
    }
    
    /**
     * 得到get方法名 SysUser --> getSysUser
     */
    public static String getGetMethodName(String str) {
        return StrUtils.upperCaseFirstCharAndAddPrefix(str, "get");
    }
    
    /**
     * 得到set方法名  SysUser --> setSysUser
     */
    public static String getSetMethodName(String str) {
        return StrUtils.upperCaseFirstCharAndAddPrefix(str, "set");
    }
    
    public static String getTableNameByVoName(String voName) {
        String tableSuffix = StringUtils.substringBeforeLast(voName, "Vo");
        String underline = StrUtils.toUnderlineAndLowerCaseByHump(tableSuffix);
        return "t_" + underline;
    }
    
    /**
     * sysUserVo.openId --> t_sys_user.open_id
     */
    public static String getTableAndField(String val) {
        String[] vals = StringUtils.split(val, Symbol.DOT);
        
        String tableSuffix = StringUtils.substringBeforeLast(vals[0], "Vo");
        String underline = StrUtils.toUnderlineAndLowerCaseByHump(tableSuffix);
        return "t_" + underline + Symbol.DOT + StrUtils.toUnderlineAndLowerCaseByHump(vals[1]);
    }
}
