/**
 * Project: yui3-common-utils
 * Class EnumUtils
 * Version 1.0
 * File Created at 2020年8月1日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.hub.utils;

import java.lang.reflect.Method;

import org.apache.commons.lang3.StringUtils;

import lombok.extern.slf4j.Slf4j;
import yui.comn.hub.model.BaseEnum;
import yui.comn.hub.model.Symbol;



/**
 * <p>
 * 枚举工具类
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
@Slf4j
public class EnumUtils {

    
    @SuppressWarnings("unchecked")
    public static final String getNm(Class<?> enCls, int cd) { 
        try {
            Method method = enCls.getMethod("values");
            BaseEnum<Integer>[] ens = (BaseEnum<Integer>[]) method.invoke(null);
            for (BaseEnum<Integer> item : ens) { 
                if (item.cd().intValue() == cd) { 
                    return item.nm();
                }
            }
        } catch (Exception e) {
            log.error("获取枚举名称失败", e);
        }
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public static final String getNm(Class<?> enCls, String cd) {
        try {
            Method method = enCls.getMethod("values");
            BaseEnum<String>[] ens = (BaseEnum<String>[]) method.invoke(null);
            for (BaseEnum<String> item : ens) {
                if (StringUtils.equals(String.valueOf(item.cd()), cd)) { 
                    return item.nm();
                }
            }
        } catch (Exception e) {
            log.error("获取枚举名称失败", e);
        }
        return null;
    }
    
    public static final String getNms(Class<?> enCls, String cd) {
        if (!StringUtils.contains(cd, Symbol.COMMA)) {
            return getNm(enCls, cd);
        } else {
            String[] cds = StringUtils.split(cd, Symbol.COMMA);
            StringBuffer buff = new StringBuffer();
            for (String c : cds) {
                if (buff.length() > 0) {
                    buff.append(Symbol.COMMA).append(Symbol.SPACE);
                }
                buff.append(getNm(enCls, c));
            }
            return buff.toString();
        }
    }
    
    @SuppressWarnings("unchecked")
    public static final Integer getCd(Class<?> enCls, String nm) { 
        try {
            Method method = enCls.getMethod("values");
            BaseEnum<Integer>[] ens = (BaseEnum<Integer>[]) method.invoke(null);
            for (BaseEnum<Integer> item : ens) { 
                if (StringUtils.equals(item.nm(), nm)) { 
                    return item.cd();
                }
            }
        } catch (Exception e) {
            log.error("获取枚举编码失败", e);
        }
        return null;
    }
    
    public static final String getCds(Class<?> enCls, String nm) {
        if (!StringUtils.contains(nm, Symbol.COMMA)) {
            return getCdStr(enCls, nm);
        } else {
            String[] nms = StringUtils.split(nm, Symbol.COMMA);
            StringBuffer buff = new StringBuffer();
            for (String c : nms) {
                if (buff.length() > 0) {
                    buff.append(Symbol.COMMA).append(Symbol.SPACE);
                }
                buff.append(getCdStr(enCls, c));
            }
            return buff.toString();
        }
    }
    
    @SuppressWarnings("unchecked")
    public static final String getCdStr(Class<?> enCls, String nm) { 
        try {
            Method method = enCls.getMethod("values");
            BaseEnum<String>[] ens = (BaseEnum<String>[]) method.invoke(null);
            for (BaseEnum<String> item : ens) { 
                if (StringUtils.equals(item.nm(), nm)) { 
                    return String.valueOf(item.cd());
                }
            }
        } catch (Exception e) {
            log.error("获取枚举编码失败", e);
        }
        return null;
    }
}
