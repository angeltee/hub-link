/**
* Project: yui3-common-hub
 * Class AbstractHubDataHandler
 * Version 1.0
 * File Created at 2018年8月14日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.data.handler;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;

import yui.comn.hub.model.HubXmlHandlerConfig;

/**
 * 抽象数据中转处理
 * @author yuyi (1060771195@qq.com)
 */ 
public abstract class AbstractHubDataHandler implements IHubDataHandler{
 
    protected abstract void doHandle(HubXmlHandlerConfig dataConfig, Map<String, Object> dataMap);
    
    @PostConstruct
    public void init() {
        if (StringUtils.isNotBlank(getRegisterName())) {
            HubDataHandlerRegister.registerHandler(getRegisterName(), this);
        }
    }
    
    @Override
    public Object handle(HubXmlHandlerConfig dataConfig, Map<String, Object> dataMap) {
        doHandle(dataConfig, dataMap);
        return null;
    }
    
    //也可以在类中直接注册
    protected String getRegisterName() {
        return null;   
    }
}
