/**
 * Project: yui3-common-hub-core
 * Class IHubDataParseHandler
 * Version 1.0
 * File Created at 2020-12-17
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.hub.data.parser.handler;

/**
 * <p>
 * 数据解析器
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public interface IHubDataParseHandler {

    Object dataParse(Object obj);
    
    Object get(Object obj, String mapper);
    
}
