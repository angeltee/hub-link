/**
 * Project: yui3-common-hub-core
 * Class IgnoreHubDataHandler
 * Version 1.0
 * File Created at 2020-12-10
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.hub.data.handler.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import yui.comn.hub.data.handler.AbstractHubDataHandler;
import yui.comn.hub.model.HubXmlHandlerConfig;

/**
 * <p>
 * 忽略某个字段
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
@Service
public class IgnoreHubDataHandler extends AbstractHubDataHandler {

    public static final String REG_NAME = "ignore";
    
    @Override
    protected void doHandle(HubXmlHandlerConfig dataConfig, Map<String, Object> dataMap) {
        dataMap.remove(dataConfig.getName());
    }

    @Override
    protected String getRegisterName() {
        return REG_NAME;
    }

    
}
