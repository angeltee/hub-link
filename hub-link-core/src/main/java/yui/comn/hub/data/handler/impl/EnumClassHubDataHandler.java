/**
 * Project: yui3-common-hub
 * Class EnumClassHubDataHandler
 * Version 1.0
 * File Created at 2020年8月1日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.hub.data.handler.impl;

import java.util.Map;

import org.apache.commons.lang3.ClassUtils;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import yui.comn.hub.data.handler.AbstractHubDataHandler;
import yui.comn.hub.model.HubConstant;
import yui.comn.hub.model.HubXmlHandlerConfig;
import yui.comn.hub.utils.EnumUtils;

/**
 * <p>
 * 枚举类处理类
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
@Slf4j
@Service
public class EnumClassHubDataHandler extends AbstractHubDataHandler {

    public static final String REG_NAME = "enCls";
    // @Autowired
    // private HubProperties hubProperties;
    
    @Override
    protected void doHandle(HubXmlHandlerConfig dataConfig, Map<String, Object> dataMap) {
    	// arg: 0 输出枚举描述， 1 表示不输出枚举描述 ,   2 表示输出枚举值
        Object obj = dataMap.get(dataConfig.getName());
        
        Class<?> enCls = dataConfig.getEnCls();
        if (null == enCls) {
            enCls = dataConfig.getEnCls();
            try {
                enCls = ClassUtils.getClass(dataConfig.getParams().get(0).toString());
            } catch (ClassNotFoundException e) {
                log.error("ClassNotFoundException " + dataConfig.getParams().get(0), e);
            }
        }
        
        if (dataConfig.getParams().size() > 1) {
        	Integer arg = Integer.valueOf(dataConfig.getParams().get(1).toString());
            if (1 == arg) {
                dataMap.put(dataConfig.getName(), EnumUtils.getNms(enCls, String.valueOf(obj)));
                return;
            } else if (2 == arg) {
            	dataMap.put(dataConfig.getName(), EnumUtils.getCds(enCls, String.valueOf(obj)));
            	return;
            }
        }
        
        String key = dataConfig.getName() + HubConstant.KEY_SUFFIX_CD;
        if (null != obj) {
            dataMap.put(key, EnumUtils.getNms(enCls, String.valueOf(obj)));
        } else {
            dataMap.put(key, null);
        }
    }

    @Override
    protected String getRegisterName() {
        return REG_NAME;
    }

    

}
