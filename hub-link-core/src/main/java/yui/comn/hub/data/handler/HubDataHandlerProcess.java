/**
* Project: yui3-common-hub
 * Class HubDataProcess
 * Version 1.0
 * File Created at 2018年8月15日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.data.handler;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import yui.comn.hub.model.HubXmlColumn;
import yui.comn.hub.model.HubXmlHandlerConfig;

/**
 * 数据加工
 * @author yuyi (1060771195@qq.com)
 */
public class HubDataHandlerProcess {
    private static HubDataHandlerProcess hubDataProcess;
    
    public static HubDataHandlerProcess getInstance() {
        if (null == hubDataProcess) {
            hubDataProcess = new HubDataHandlerProcess();
        }
        return hubDataProcess;
    }

    public void handle(HubXmlColumn col, Map<String, Object> dataMap) {
        List<HubXmlHandlerConfig> configs = col.getHandlerConfigs();
        
        // 原始的值
        Object origVal = dataMap.get(col.getName());
        
        // 轮训所有方法
        for (HubXmlHandlerConfig dataConfig : configs) {
            
            IHubDataHandler hubDataHandler = HubDataHandlerRegister.getHandler(dataConfig.getHandler());
            hubDataHandler.handle(dataConfig, dataMap);
//            Object resultVal = hubDataHandler.handle(dataConfig.getParams(), dataMap);
//            dataMap.put(col.getName(), resultVal);
        }
        
        /*if (StringUtils.isBlank(col.getToName())) {
            dataMap.put(col.getName(), dataMap.get(col.getName()));
        } else {
            dataMap.put(col.getToName(), dataMap.get(col.getName()));
            dataMap.put(col.getName(), origVal);
        }*/
        
        if (StringUtils.isNotBlank(col.getToName())) {
            dataMap.put(col.getToName(), dataMap.get(col.getName()));
            dataMap.put(col.getName(), origVal);
        }
        
    }
    
}
