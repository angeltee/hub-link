/**
* Project: yui3-common-hub
 * Class TestHubDataHandler
 * Version 1.0
 * File Created at 2018年8月14日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.data.handler.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import yui.comn.hub.data.handler.AbstractHubDataHandler;
import yui.comn.hub.model.HubXmlHandlerConfig;

/**
 * $测试处理类
 * @author yuyi (1060771195@qq.com)
 */
@Service
public class TestHubDataHandler extends AbstractHubDataHandler {

    @Override 
    protected void doHandle(HubXmlHandlerConfig dataConfig, Map<String, Object> dataMap) {
        System.out.println("TestHubDataHandler");
//    	Object obj = dataMap.get(dataConfig.getName());
//    	if (null != obj) {
//    		dataMap.put(dataConfig.getName(), obj.toString() + "-test");
//    	}
    }

	@Override
	protected String getRegisterName() {
		return "test";
	}
    
}
