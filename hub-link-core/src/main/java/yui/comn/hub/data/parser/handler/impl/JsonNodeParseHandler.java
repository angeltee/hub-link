/**
 * Project: yui3-common-hub-core
 * Class JsonNodeParseHandler
 * Version 1.0
 * File Created at 2020-12-17
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.hub.data.parser.handler.impl;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;

import yui.comn.hub.data.parser.handler.AbstractHubDataParseHandler;

/**
 * <p>
 * jsonson jsonNode
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
@Service
public class JsonNodeParseHandler extends AbstractHubDataParseHandler {

    public static final String REG_NAME = "jsonNode";

    @Override
    protected String getRegisterName() {
        return REG_NAME;
    }
    
    
    @Override
    public Object dataParse(Object obj) {
        if (obj instanceof JsonNode) {
            return obj;
        }
        try {
            return getObjectMapper().valueToTree(obj);
        } catch (Exception e) {
            String errMsg = "valueToTree error";
            log.error(errMsg, e);
            throw new RuntimeException(errMsg, e);
        }
    }

    @Override
    protected Object getObjByAttr(Object obj, String attrName) {
        return ((JsonNode) obj).get(attrName);
    }

    @Override
    protected Object getAttrValue(Object obj, String attrName) {
        return ((JsonNode) obj).get(attrName);
    }

}
