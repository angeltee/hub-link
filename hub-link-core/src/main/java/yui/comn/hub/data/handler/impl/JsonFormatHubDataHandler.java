/**
 * Project: yui3-common-hub
 * Class JsonFormatHubDataHandler
 * Version 1.0
 * File Created at 2019年4月7日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.hub.data.handler.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import yui.comn.hub.data.handler.AbstractHubDataHandler;
import yui.comn.hub.model.HubXmlHandlerConfig;

/**
 * <p>
 * 日期格式
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
@Service
public class JsonFormatHubDataHandler extends AbstractHubDataHandler {

    public static final String REG_NAME = "jsonFormat";
    
    
    @Override
    protected void doHandle(HubXmlHandlerConfig dataConfig, Map<String, Object> dataMap) {
        List<Object> params = dataConfig.getParams();
        
        Date date = (Date) dataMap.get(dataConfig.getName());
        
        if (null != date) {
            String pattern = (String) params.get(0);
            dataMap.put(dataConfig.getName(), format(date, pattern));
        }
    }
    
    private String format(Date date, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(date);
    }

    @Override
    protected String getRegisterName() {
        return REG_NAME;
    }
    
    
}
