/**
* Project: yui3-common-hub
 * Class HubXmlCache
 * Version 1.0
 * File Created at 2018年8月10日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.xml.parser;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import yui.comn.hub.model.HubXmlColumn;
import yui.comn.hub.model.Symbol;
import yui.comn.hub.xml.plugins.DefaultHubXmlPlugin;
import yui.comn.hub.xml.plugins.HubXmlPlugin;

/**
 * xml 配置文件解析后缓存
 * @author yuyi (1060771195@qq.com)
 */
public class HubXmlCache {
    // grid kv map缓存 
    private Map<String, Object> gridKvMapCache = new HashMap<>();
    // grid list缓存 
    private Map<String, List<HubXmlColumn>> gridListCache = new HashMap<>();
    // search list缓存 
    private Map<String, Map<String, HubXmlColumn>> searchMapCache = new HashMap<>();
    // search map缓存 
    private Map<String, Map<String, String>> searchCache = new HashMap<>();
    // grid field kv map缓存 
    private Map<String, Object> gridFieldKvMapCache = new HashMap<>();
    //xml解析器
    private HubXmlParser hubXmlParser; // = new HubXmlParser(new DefaultHubXmlPlugin());
    
    private HubXmlPlugin hubXmlPlugin;
    /** 是否开启缓存 */
    public boolean cache = true;
    
    private static HubXmlCache hubXmlCache = new HubXmlCache();
    
    public static HubXmlCache getInstance() {
        return hubXmlCache;
    }
    
    public void initHubXmlParser(HubXmlPlugin hubXmlPlugin) {
        if (null != hubXmlPlugin) {
            hubXmlParser = new HubXmlParser(hubXmlPlugin);
        } else {
            hubXmlParser = new HubXmlParser(new DefaultHubXmlPlugin());
        }
    }
    
    private HubXmlParser getHubXmlParser() {
        if (null == hubXmlParser) {
            hubXmlParser = new HubXmlParser(new DefaultHubXmlPlugin());
        }
        return hubXmlParser;
    }
    
    /**
     * 缓存Map中的key是由Controller类名加上name
     */
    private String spliceKey(Class<?> clazz, String name) {
        return new StringBuffer().append(clazz.getSimpleName()).append(Symbol.DOT).append(name).toString();
    }
    
    public Map<String, String> toSearch(Class<?> clazz, String name) {
        String searchKey = spliceKey(clazz, name);
        
        Map<String, String> searchMap = searchCache.get(searchKey);
        if (null == searchMap) {
            searchMap = getHubXmlParser().parseXmlSearchColToMap(clazz, name);
            if (isCache()) {
                searchCache.put(searchKey, searchMap);
            }
        }
        return searchMap;
    }
    
    public Map<String, HubXmlColumn> toSearchMap(Class<?> clazz, String name) {
        String gridKey = spliceKey(clazz, name);
        
        Map<String, HubXmlColumn> searchMap = searchMapCache.get(gridKey);
        if (null == searchMap) {
        	searchMap = new LinkedHashMap<>();
        	List<HubXmlColumn> searchList = getHubXmlParser().parseXmlSearchColToList(clazz, name);
        	for (HubXmlColumn hubXmlColumn : searchList) {
        		searchMap.put(hubXmlColumn.getName(), hubXmlColumn);
			}
            if (isCache()) {
            	searchMapCache.put(gridKey, searchMap);
            }
        }
        return searchMap;
    }
    
    @SuppressWarnings("unchecked")
    public Map<String, Object> toGridKvMap(Class<?> clazz, String name) {
        String gridKey = spliceKey(clazz, name);
        
        Map<String, Object> gridKvMap = (Map<String, Object>) gridKvMapCache.get(gridKey);
        if (null == gridKvMap) {
            gridKvMap = getHubXmlParser().parseXmlGridColToKvMap(clazz, name);
            if (isCache()) {
                gridKvMapCache.put(gridKey, gridKvMap);
            }
        }
        return gridKvMap;
    }
    
    public List<HubXmlColumn> toGridList(Class<?> clazz, String name) {
        String gridKey = spliceKey(clazz, name);
        
        List<HubXmlColumn> gridList = gridListCache.get(gridKey);
        if (null == gridList) {
            gridList = getHubXmlParser().parseXmlGridColToList(clazz, name);
            if (isCache()) {
                gridListCache.put(gridKey, gridList);
            }
        }
        return gridList;
    }
    
    @SuppressWarnings("unchecked")
    public Map<String, String> toGridFieldKvList(Class<?> clazz, String name) {
        String gridKey = spliceKey(clazz, name);
        
        Map<String, String> gridFieldKvMap = (Map<String, String>) gridFieldKvMapCache.get(gridKey);
        if (null == gridFieldKvMap) {
            gridFieldKvMap = getHubXmlParser().parseXmlGridColToFieldKvMap(clazz, name);
            if (isCache()) {
                gridFieldKvMapCache.put(gridKey, gridFieldKvMap);
            }
        }
        return gridFieldKvMap;
    }

    
    public boolean isCache() {
        return cache;
    }

    public void setCache(boolean cache) {
        this.cache = cache;
    }

    public HubXmlPlugin getHubXmlPlugin() {
        return hubXmlPlugin;
    }

    public void setHubXmlPlugin(HubXmlPlugin hubXmlPlugin) {
        this.hubXmlPlugin = hubXmlPlugin;
    }
    
}
