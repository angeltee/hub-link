/**
 * Project: yui3-common-hub-core
 * Class DefaultHubXmlPlugin
 * Version 1.0
 * File Created at 2020-12-14
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.hub.xml.plugins;

import java.lang.annotation.Annotation;

import yui.comn.hub.model.HubXmlColumn;

/**
 * <p>
 * 默认插件
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class DefaultHubXmlPlugin implements HubXmlPlugin {

    // @SuppressWarnings("rawtypes")
    @Override
    public void xmlHandle(Annotation[] annos, HubXmlColumn col) {
        // TODO 已经使用json在取值得时候进行处理，如果需要可以进行spring注入开启
        return;
        /*if (ArrayUtils.isEmpty(annos)) {
            return;
        }
        for (Annotation anno : annos) {
            if (anno.annotationType().isAssignableFrom(JsonSerialize.class)) {
                Class<? extends JsonSerializer> using = ((JsonSerialize) anno).using();
                if (using.isAssignableFrom(ToStringSerializer.class)) {
                    col.addHandler(ToStringHubDataHandler.REG_NAME);
                }
            } else if (anno.annotationType().isAssignableFrom(JsonIgnore.class)) {
                if (null != anno) {
                    col = null;
                }
            } else if (anno.annotationType().isAssignableFrom(JsonFormat.class)) {
                String pattern = ((JsonFormat) anno).pattern();
                col.addHandler("jsonFormat(" + pattern + ")");
            }
        }*/
    }

}
