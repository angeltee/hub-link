/**
* Project: yui3-common-hub
 * Class HubJavaType
 * Version 1.0
 * File Created at 2018年8月17日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.model;

import java.util.HashMap;
import java.util.Map;

/**
 * hub java 类型
 * @author yuyi (1060771195@qq.com)
 */
public enum HubJavaType {
 
    TYPE_TIMESTAMP("java.sql.Timestamp"),
    TYPE_SQL_DATE("java.sql.Date"),
    TYPE_DATE("java.util.Date"),
    TYPE_BIGDECIMAL("java.math.BigDecimal"),
    TYPE_LONG("java.lang.Long"),
    TYPE_BASE_LONG("long"),
    TYPE_INT("java.lang.Integer"),
    TYPE_BASE_INT("int"),
    TYPE_DOUBLE("java.lang.Double"),
    TYPE_BASE_DOUBLE("double"),
    TYPE_FLOAT("java.lang.Float"),
    TYPE_BASE_FLOAT("float"),
    TYPE_BOOLEAN("java.lang.Boolean"),
    TYPE_BASE_BOOLEAN("boolean"),
    TYPE_STRING("java.lang.String"),
    ;
    
    private String name;
    
    private static Map<String, HubJavaType> javaTypeMap;
    
    public static HubJavaType getJavaTypeByName(String name) {
        if (null == javaTypeMap) {
            javaTypeMap = new HashMap<>();
            for (HubJavaType javaType : HubJavaType.values()) {
                javaTypeMap.put(javaType.getName(), javaType);
            }
        }
        return javaTypeMap.get(name);
    }
    
    HubJavaType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
