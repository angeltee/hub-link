/**
* Project: yui3-common-hub
 * Class HubDataHandlerConfig
 * Version 1.0
 * File Created at 2018年8月15日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.model;

import java.util.List;

import lombok.Data;

/**
 * 处理定义类
 * @author yuyi (1060771195@qq.com)
 */
@Data
public class HubXmlHandlerConfig {

    private String name;
    private String handler;
    private List<Object> params;
    private Class<?> enCls;
    
    public HubXmlHandlerConfig() {
    }
    
    public HubXmlHandlerConfig(String name) {
        this.name = name;
    }
    
    public HubXmlHandlerConfig(String name, String handler) {
        this.name = name;
        this.handler = handler;
    }
    
    public HubXmlHandlerConfig(String name, String handler, List<Object> params) {
        this.name = name;
        this.handler = handler;
        this.params = params;
    }

}
