/**
 * Project: yui3-common-hub
 * Class HubConstant
 * Version 1.0
 * File Created at 2020年8月3日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.hub.model;

/**
 * <p>
 * hub常量
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class HubConstant {

    public final static String KEY_SUFFIX_CD = "Dsr";
    
    public final static String KEY_SUFFIX_DSR = "描述";
    
}
