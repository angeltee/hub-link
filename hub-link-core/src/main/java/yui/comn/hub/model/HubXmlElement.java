/**
* Project: yui3-common-hub
 * Class HubXmlType
 * Version 1.0
 * File Created at 2018年8月10日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.model;

/**
 * xml 类型
 * @author yuyi (1060771195@qq.com)
 */
public enum HubXmlElement {
    grid, search, col, collection
}
 