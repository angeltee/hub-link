/**
* Project: yui3-common-hub
 * Class SqlConstant
 * Version 1.0
 * File Created at 2017年10月1日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.model;

/**
 * @author yuyi (1060771195@qq.com)
 */
public class Symbol {

    /**
     * 左圆括号
     */
    public static final String OPNE_PAREN = "(";

    /**
     * 右圆括号
     */
    public static final String CLOSE_PAREN = ")";
    
    /**
     * 空
     */ 
    public static final String EMPTY = "";
    
    /**
     * 空格
     */ 
    public static final String SPACE = " ";
    
    /**
     * 百分号
     */ 
    public static final String PERCENT_SIGN = "%";
    
    /**
     * 双引号
     */ 
    public static final String DOUBLE_QUOTATION = "\"";
    
    /**
     * 单引号
     */ 
    public static final String SINGLE_QUOTATION = "'";
    
    /**
     * BETWEEN
     */ 
    public static final String BETWEEN = "between";
    
    /**
     * LIKE
     */ 
    public static final String LIKE = "like";
    
    /**
     * AND
     */ 
    public static final String AND = "and";
    
    /**
     * OR
     */ 
    public static final String OR = "or";

    /**
     * 逗号
     */ 
    public static final String COMMA = ",";

    /**
     * 点号
     */ 
    public static final String DOT = ".";
    
    /**
     * 点号分隔符
     */ 
    public static final String DOT_DELIM = "\\.";
    
    /**
     * 冒号
     */ 
    public static final String COLON = ":";
    
    /**
     * 分号
     */ 
    public static final String SEMICOLON = ";";
    
    /**
     * 竖号
     */ 
    public static final String PIPE = "|";
    
}
