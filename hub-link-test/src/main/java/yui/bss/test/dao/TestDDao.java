package yui.bss.test.dao;

import org.apache.ibatis.annotations.Mapper;
import yui.bss.test.dto.TestDDto;
import yui.bss.test.vo.TestDVo;
import yui.comn.mybatisx.core.mapper.BaseDao;

/**
 * <p>
 * 测试E Mapper 接口
 * </p>
 *
 * @author yui
 */
@Mapper
public interface TestDDao extends BaseDao<TestDVo, TestDDto> {

}
