package yui.bss.test.dao;

import org.apache.ibatis.annotations.Mapper;
import yui.bss.test.dto.TestBDto;
import yui.bss.test.vo.TestBVo;
import yui.comn.mybatisx.core.mapper.BaseDao;

/**
 * <p>
 * 测试B Mapper 接口
 * </p>
 *
 * @author yui
 */
@Mapper
public interface TestBDao extends BaseDao<TestBVo, TestBDto> {

}
