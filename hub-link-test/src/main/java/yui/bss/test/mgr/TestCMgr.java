package yui.bss.test.mgr;

import yui.bss.test.dto.TestCDto;
import yui.bss.test.vo.TestCVo;
import yui.comn.mybatisx.extension.mgr.IMgr;

/**
 * <p>
 * 测试C
 * </p>
 *
 * @author yui
 */
public interface TestCMgr extends IMgr<TestCVo, TestCDto> {

}
