package yui.bss.test.ro;

import java.io.Serializable;

import lombok.Data;

/**
 * <p>
 * 测试B 接收类
 * </p>
 *
 * @author yui
 */

@Data
public class TestBRo implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Long id;                                  // ID                                 
    private Long aId;                                 // aID                                 
    private Long cId;                                 // cID                                 


}
