package yui.bss.test.ro;

import java.io.Serializable;

import lombok.Data;

/**
 * <p>
 * 测试E 接收类
 * </p>
 *
 * @author yui
 */

@Data
public class TestDRo implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Long id;                                  // ID                                 
    private Long aId;                                 // aID                                 


}
