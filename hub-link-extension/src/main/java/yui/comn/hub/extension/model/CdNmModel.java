package yui.comn.hub.extension.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 编码名称
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CdNmModel implements Serializable {
    private static final long serialVersionUID = 3592746869595760244L;
    
    private Object cd;
    private String nm;
    
}
