/**
 * Project: yui3-common-utils
 * Class SearchMediaWrapper
 * Version 1.0
 * File Created at 2019年12月9日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.hub.extension.model;

import java.io.Serializable;

import lombok.Data;

/**
 * <p>
 * 阿里云视频点播包装类
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
@Data
public class VodWrapper implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private static String fields2 = "Title,CoverURL,Status,Size,MediaType,"
            + "MediaSource,Tags,CreationTime,ModificationTime,"
            // + "CatId,CatName,StorageLocation,PlayInfoList,Snapshots,SpriteSnapshots,"
            + "CatId,CatName,StorageLocation,"
            + "DownloadSwitch,PreprocessStatus,Duration"; 
    
    private int pageNo;
    private int pageSize;
    private StringBuffer sortBy;
    private StringBuffer match;
    private String fields;
    private String searchType;
    
    public VodWrapper() {
        this.pageNo = 1;
        this.pageSize = 10;
        this.sortBy = new StringBuffer();
        this.match = new StringBuffer();
        this.fields = fields2;
    }
    
    public StringBuffer getSortByBuffer() {
        return this.sortBy;
    }
    
    public StringBuffer getMatchBuffer() {
        return this.match;
    }
    
    public String getSortBy() {
        return this.sortBy.toString();
    }
    
    public String getMatch() {
        return this.match.toString();
    }
}
