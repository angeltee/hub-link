/**
* Project: yui3-common-hub
 * Class TableNote
 * Version 1.0
 * File Created at 2018年8月12日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

/**
 * vo 对应的 表名
 * @author yuyi (1060771195@qq.com)
 */
@Target({ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TableNote {
    
    /**
     * 默认注释
     * @return
     */
    @AliasFor("tableName")
    String value() default "";
    
    /**
     * 默认注释
     * @return
     */
    @AliasFor("value")
    String tableName() default "";
    
}
