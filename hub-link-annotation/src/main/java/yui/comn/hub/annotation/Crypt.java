package yui.comn.hub.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

import yui.comn.hub.model.CryptMode;
import yui.comn.hub.model.CryptType;

/**
 * 加解密注释
 * @author yuyi (1060771195@qq.com)
 */
@Target({ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Crypt {

    /**
     * 默认注释
     * @return
     */
    @AliasFor("cryptType")
    CryptType value() default CryptType.NULL;
    
    /**
     * 加解密类型
     */
    CryptType cryptType() default CryptType.NULL;
    
    /**
     * 加解密模式
     */
    CryptMode cryptMode() default CryptMode.NULL;
    
    /**
     * 解密
     */
    boolean decrypt() default true;
    
    /**
     * 加密
     */
    boolean encrypt() default true;
    
    
}
