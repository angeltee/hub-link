# 简介

[Hub Link](https://easy4use.cn)（简称HL）是数据加工中心，作为[Mybatis Link](https://easy4use.cn)（简称ML）的增强工具，可以就像咖啡伴侣一样，在使用ML的时候更加丝滑。HL作为数据加工中心，会对进出数据进行加工处理，接下来会结合Mybatis-link进行处理。

## 特性

- **无侵入:** 采用插件的形式，对进出数据进行加功处理。
- **插件:** 可以作为Mybatis-Link的增强工具，两者可以一起使用。
- **请求数据:** 每个controller都会对应一个xml，通过xml配置对请求数据进行加功处理。
- **返回数据:** 每个controller都会对应一个xml，通过xml配置对返回数据进行加功处理。

## 代码托管

> **[Gitee](https://gitee.com/easy4use/hub-link)**

## 参与贡献

欢迎各路好汉一起来参与完善 Hub-Link，我们期待你的 PR！

- 贡献代码：代码地址 [Hub-Link](https://gitee.com/easy4use/hub-link) ，欢迎提交 Issue 或者 Pull Requests
